package com.example.manoj.upload_leaf;

import android.support.test.rule.ActivityTestRule;
import android.view.View;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

public class MainActivityTest {
@Rule
public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule(MainActivity.class);

private MainActivity mActivity=null;
    @Before
    public void setUp() throws Exception {
        mActivity=mActivityTestRule.getActivity();
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }

    @Test
    public void onCreate() {
        View view =mActivity.findViewById(R.id.uploadBtn);
        assertNotNull("MainActivity is launched successfully",view);
    }

    @Test
    public void onCreateOptionsMenu() {
    }

    @Test
    public void onOptionsItemSelected() {
    }

    @Test
    public void onClick() {
    }

    @Test
    public void onActivityResult() {
    }

    @Test
    public void getItems() {
    }

    @Test
    public void handleFromGallery() {
    }

    @Test
    public void handleFromCamera() {
    }

    @Test
    public void isGrantExternalRW() {
    }
    @Test
    public void testOnImageChooseButtonClicked()
    {
        assertNotNull(mActivity.findViewById(R.id.chooseBtn));
        onView(withId(R.id.chooseBtn)).perform(click());

    }
}