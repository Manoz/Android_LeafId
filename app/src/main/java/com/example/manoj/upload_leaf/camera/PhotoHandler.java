package com.example.manoj.upload_leaf.camera;

import android.app.Activity;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PhotoHandler {
    private final Activity activity;
    private final String DIR_NAME="LeafId";
    private final String FILE_EXTENSION=".jpg";
    private final String FILE_LABEL_NAME="leafid_";
    private final String DATE_FORMAT="yyyymmddhhmmss";
    private byte[] imgData;


    public PhotoHandler(Activity activity) {
        this.activity = activity;
    }


    public Uri saveImage(byte[] data) {
        this.imgData=data;

        if(this.imgData==null)
            Toast.makeText(this.activity,"null image from cameras",Toast.LENGTH_LONG).show();

        File pictureFileDir = getDir();

        if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {

            Log.d("err", "Can't create directory to save image.");
            Toast.makeText(activity, "Can't create directory to save image.",
                    Toast.LENGTH_LONG).show();
        }
        String filename = pictureFileDir.getPath() + File.separator + getImageNameByDate();

        return saveImageToFile(data,filename);
    }

    private File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, DIR_NAME);
    }


    private String getImageNameByDate()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        String date = dateFormat.format(new Date());
        String photoFile = FILE_LABEL_NAME + date + FILE_EXTENSION;
        return photoFile;

    }
    private Uri saveImageToFile(byte[] data,String filename)
    {
        File pictureFile = new File(filename);


        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();
            //Toast.makeText(activity, "New Image saved:" + filename,Toast.LENGTH_LONG).show();
        } catch (Exception error) {
            Log.d("err", "File" + filename + "not saved: "
                    + error.getMessage());
            Toast.makeText(activity, "Image could not be saved.",
                    Toast.LENGTH_LONG).show();
        }
        return Uri.fromFile(pictureFile);
    }


}
