package com.example.manoj.upload_leaf;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.manoj.upload_leaf.camera.PhotoHandler;
import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;

public class Camerademo extends AppCompatActivity {

    private byte[] mCameraData;
    private ImageView ic_clickbtn,ic_flash,ic_cancel,ic_swapcamera;
    private CameraView cameraView;


    private CameraKitEventListener cameraKitEventListener=new CameraKitEventListener() {
        @Override
        public void onEvent(CameraKitEvent cameraKitEvent) {

        }

        @Override
        public void onError(CameraKitError cameraKitError) {

        }

        @Override
        public void onImage(CameraKitImage cameraKitImage) {
            mCameraData=cameraKitImage.getJpeg();
            if (mCameraData != null) {
                Uri uri= saveImage();
                Intent resultIntent = new Intent();
                resultIntent.setData(uri);
                setResult(Activity.RESULT_OK, resultIntent);
            } else {
                setResult(Activity.RESULT_CANCELED);
            }
            finish();
        }

        @Override
        public void onVideo(CameraKitVideo cameraKitVideo) {

        }
    };

    private View.OnClickListener onPictureSnapped = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            cameraView.captureImage();
        }
    };

    private View.OnClickListener onFlashBtnClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            cameraView.toggleFlash();
        }
    };
    private View.OnClickListener onCameraSwappedBtnClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            cameraView.toggleFacing();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_camerademo);

        ic_clickbtn=findViewById(R.id.ic_clickbtn);
        ic_flash=findViewById(R.id.ic_flash);
        ic_cancel=findViewById(R.id.ic_cancel);
        ic_swapcamera=findViewById(R.id.ic_swapcamera);
        cameraView = findViewById(R.id.camera);

        cameraView.addCameraKitListener(cameraKitEventListener);
        ic_clickbtn.setOnClickListener(onPictureSnapped);
        ic_flash.setOnClickListener(onFlashBtnClicked);
        ic_swapcamera.setOnClickListener(onCameraSwappedBtnClicked);


    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraView.start();
    }

    @Override
    protected void onPause() {
        cameraView.stop();
        super.onPause();
    }

    public Uri saveImage()
    {
        PhotoHandler handler = new PhotoHandler(Camerademo.this);
        return handler.saveImage(mCameraData);
    }

}