package com.example.manoj.upload_leaf.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.manoj.upload_leaf.R;
import com.example.manoj.upload_leaf.model.Plant;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PlantAdapter extends ArrayAdapter<Plant>{
    List<Plant> list ;
    Context context;
    ViewHolder holder;


    public PlantAdapter(@NonNull Context context,List<Plant> incomingList) {
        super(context, 0);
        this.context=context;
        this.list=incomingList;
    }


    @Override
    public int getCount() {
        return list==null?0:list.size();
    }

    @Nullable
    @Override
    public Plant getItem(int position) {
        return list.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final Plant plant =getItem(position);
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.singleitemdatabase_temp,parent,false);
        }

        holder = new ViewHolder();
        holder.commonName = convertView.findViewById(R.id.common_name);
        holder.matching_percent =  convertView.findViewById(R.id.matching_percent);
        holder.img_plant=convertView.findViewById(R.id.img_plant);
        holder.imgCount=convertView.findViewById(R.id.imgCount);
        holder.txtFamilyAndCommonName =convertView.findViewById(R.id.txtFamilyAndCommonName);

        holder.txtFamilyAndCommonName.setText(plant.getFamily()+", "+plant.getGenus());
        holder.commonName.setText( plant.getCommonName());
        holder.matching_percent.setText( plant.getFamily());
        holder.imgCount.setText(plant.getGallery().size()+"");
        Picasso.get().load(plant.getImageUrl())
                .placeholder(R.drawable.ic_launcher_background)
                .into(holder.img_plant);

        return convertView;
    }
    static class ViewHolder {
         ImageView img_plant;
         TextView commonName;
         TextView matching_percent;
         TextView imgCount;
         TextView txtFamilyAndCommonName;
    }
    public Context getContext()
    {
        return this.context;
    }
}
