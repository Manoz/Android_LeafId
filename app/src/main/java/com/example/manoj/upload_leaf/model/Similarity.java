package com.example.manoj.upload_leaf.model;

public class Similarity {
String similarity;
Plant plant;

    public String getSimilarity() {
        return similarity;
    }

    public void setSimilarity(String similarity) {
        this.similarity = similarity;
    }

    public Plant getPlant() {
        return plant;
    }

    public void setPlant(Plant plant) {
        this.plant = plant;
    }
    public String toString()
    {

        return ("similarity ="+this.similarity+"\n"+
                plant.toString());
    }
}
