package com.example.manoj.upload_leaf.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.manoj.upload_leaf.R;
import com.example.manoj.upload_leaf.SinglePhotoDetailActivity;
import com.squareup.picasso.Picasso;

public class ImageGalleryAdapter extends RecyclerView.Adapter<ImageGalleryAdapter.MyViewHolder>{
    private String[] galleryImages;
    private Context mContext;

    public ImageGalleryAdapter(Context context, String[] galleryImages) {
        mContext = context;
        this.galleryImages = galleryImages;
    }
    @Override
    public ImageGalleryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View photoView = inflater.inflate(R.layout.singlegallerytemplate, parent, false);
        ImageGalleryAdapter.MyViewHolder viewHolder = new ImageGalleryAdapter.MyViewHolder(photoView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ImageGalleryAdapter.MyViewHolder holder, final int position) {

        String picture = galleryImages[position];
        ImageView imageView = holder.mPhotoImageView;

        //show using image view
        Picasso.get().load(picture)
                .placeholder(R.drawable.ic_launcher_background)
                .into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SinglePhotoDetailActivity.class);
                intent.putExtra("index",position);
                intent.putExtra("images",galleryImages);
                mContext.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        if((this.galleryImages==null)||(this.galleryImages.length==0)){
            Toast.makeText(this.mContext,"No Images were found", Toast.LENGTH_LONG).show();
            return 0;
        }
        return (galleryImages.length);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView mPhotoImageView;

        public MyViewHolder(View itemView) {

            super(itemView);
            mPhotoImageView =  itemView.findViewById(R.id.iv_photo);

        }
    }


}