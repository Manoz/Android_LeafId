package com.example.manoj.upload_leaf.interfaces;

public interface OnCameraPictureReceived {
    byte[] getImage();
}
