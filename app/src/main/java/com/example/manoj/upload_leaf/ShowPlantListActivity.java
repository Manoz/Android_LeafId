package com.example.manoj.upload_leaf;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.manoj.upload_leaf.ParserHelper.NetworkResponseParser;
import com.example.manoj.upload_leaf.model.Plant;
import com.example.manoj.upload_leaf.adapters.PlantAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class ShowPlantListActivity extends AppCompatActivity {

    List<Plant> plantList;
    PlantAdapter plantAdapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_plant_list);

        listView =  findViewById(R.id.plantListView);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("epuzzle");

            try{
                NetworkResponseParser parser = new NetworkResponseParser();
                JSONArray jsonArr = new JSONArray(value);
                plantList = parser.getPlantListFromJsonArray(jsonArr);
                plantAdapter = new PlantAdapter(this,plantList);

                listView.setAdapter(plantAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Plant plant = plantAdapter.getItem(position);
                        Intent i = new Intent(plantAdapter.getContext(), SinglePlantIDetailViewActivity.class);
                        i.putExtra("id", plant.getId());
                        startActivity(i);
                    }
                });
            }
            catch(JSONException e){
                System.err.println(e.getLocalizedMessage());
            }
        }
        else{
            System.err.println("Could not parse JSON value");
        }
    }

}
