package com.example.manoj.upload_leaf;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.manoj.upload_leaf.ParserHelper.NetworkResponseParser;
import com.example.manoj.upload_leaf.adapters.PagerAdapter;
import com.example.manoj.upload_leaf.interfaces.OnPlantDataReceivedListener;
import com.example.manoj.upload_leaf.model.Plant;
import com.example.manoj.upload_leaf.util.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class SinglePlantIDetailViewActivity extends AppCompatActivity implements OnPlantDataReceivedListener{
    private String getAllURL = "https://plantrecognition.herokuapp.com/api/find/";
    private String id;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Plant plant;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_item_view);

        this.tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Details"));
        tabLayout.addTab(tabLayout.newTab().setText("Gallery"));
        tabLayout.addTab(tabLayout.newTab().setText("Wiki"));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.id = extras.getString("id");
        }
        else
            this.id="zU1ESGvfJx";

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
         this.viewPager = findViewById(R.id.pager);
        makeNetworkCall();

    }

    private void makeNetworkCall()
    {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, getAllURL+id, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            NetworkResponseParser parser = new NetworkResponseParser();
                            plant=parser.parseSinglePlant(response);
                            updateView();
                        }
                        catch(JSONException e){}
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error

                    }
                });
        MySingleton
                .getInstance(getApplicationContext())
                .addToRequestQueue(jsonObjectRequest);
    }
    private void updateView()
    {
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    @Override
    public Plant getPlantData(){
        return this.plant;
    }
}
