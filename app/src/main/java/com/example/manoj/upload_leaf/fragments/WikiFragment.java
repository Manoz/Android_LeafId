package com.example.manoj.upload_leaf.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.manoj.upload_leaf.R;
import com.example.manoj.upload_leaf.interfaces.OnPlantDataReceivedListener;
import com.example.manoj.upload_leaf.model.Plant;

/**
 * A simple {@link Fragment} subclass.
 */
public class WikiFragment extends Fragment {
private Plant plant;
private OnPlantDataReceivedListener plantDataReceivedListener;
private String wikiLink="https://en.wikipedia.org/wiki/";
    public WikiFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            plantDataReceivedListener = (OnPlantDataReceivedListener) activity;
            this.plant=plantDataReceivedListener.getPlantData();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String url=this.wikiLink+plant.getGenus().toLowerCase()+"_"+plant.getSpecies().toLowerCase();

        View view =inflater.inflate(R.layout.wikipediafragment, container, false);
        WebView wikiView = view.findViewById(R.id.wikiview);
        wikiView.setWebViewClient(new MyBrowser());
        wikiView.getSettings().setLoadsImagesAutomatically(true);
        wikiView.getSettings().setJavaScriptEnabled(true);
        wikiView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wikiView.loadUrl(url);

        return view;
    }
    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
