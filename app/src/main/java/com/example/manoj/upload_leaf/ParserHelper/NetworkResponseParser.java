package com.example.manoj.upload_leaf.ParserHelper;

import android.util.Log;
import android.widget.Toast;

import com.example.manoj.upload_leaf.model.Plant;
import com.example.manoj.upload_leaf.model.Similarity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;


public class NetworkResponseParser {






    public Plant parseSinglePlant(JSONObject plant_obj) throws JSONException
    {
        Plant plant = new Plant();

        plant.setId(plant_obj.getString("id"));
        plant.setImageUrl(plant_obj.getString("imageUrl"));
        plant.setKingdom(plant_obj.getString("kingdom"));
        plant.setSubkingdom(plant_obj.getString("subKingdom"));
        plant.setSuperDivision(plant_obj.getString("superDivision"));
        plant.setDivision(plant_obj.getString("division"));
        plant.setPhylum(plant_obj.getString("phylum"));
        plant.setClassName(plant_obj.getString("className"));
        plant.setSubClass(plant_obj.getString("subClass"));
        plant.setOrder(plant_obj.getString("order"));
        plant.setFamily(plant_obj.getString("family"));
        plant.setGenus(plant_obj.getString("genus"));
        plant.setSpecies(plant_obj.getString("species"));
        plant.setCommonName(plant_obj.getString("commonName"));
        plant.setDescription(plant_obj.getString("description"));





        JSONArray galleryArr=plant_obj.getJSONArray("albums");
        if((galleryArr==null)||(galleryArr.length()==0))
        {
            Log.d("Gallery is nulll", galleryArr.toString());
        }
        else{
            plant.setGallery(handleGalleryOfPlant(galleryArr));
        }

        return plant;

    }
    private Similarity parseSingleSimilarity(JSONObject jo) throws JSONException
    {

        Similarity similarity = new Similarity();

        similarity.setSimilarity(jo.getString("similar"));
        similarity.setPlant(parseSinglePlant(jo.getJSONObject("plant")));

        return similarity;
    }
    public List<Plant> getPlantListFromJsonArray(JSONArray rawPlants) throws JSONException
    {
        List<Plant> plantList = new LinkedList<>();
        for(int index=0;index<rawPlants.length();index++)
        {
            plantList.add(parseSinglePlant(rawPlants.getJSONObject(index)));
        }
        return plantList;
    }

    public List<Similarity> getSimilarPlantList(JSONArray rawSimilarPlants) throws JSONException
    {
        List<Similarity> similarList = new LinkedList<>();
        for(int index=0;index<rawSimilarPlants.length();index++)
        {
            similarList.add(parseSingleSimilarity(rawSimilarPlants.getJSONObject(index)));
        }
        return similarList;
    }

    private List<String> handleGalleryOfPlant(JSONArray rawGallery) throws JSONException
    {
        List<String> galleryList = new LinkedList<String>();
        if((rawGallery == null)||(rawGallery.length()==0)) {
            return null;
        }
        for(int galleryIndex = 0; galleryIndex<rawGallery.length(); galleryIndex++)
        {
            galleryList.add(rawGallery.getString(galleryIndex));
        }
        return galleryList;
    }
}

