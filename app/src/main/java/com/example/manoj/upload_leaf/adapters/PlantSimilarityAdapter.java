package com.example.manoj.upload_leaf.adapters;

import android.content.Context;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.manoj.upload_leaf.R;
import com.example.manoj.upload_leaf.model.Plant;
import com.example.manoj.upload_leaf.model.Similarity;
import com.squareup.picasso.Picasso;

import java.util.List;

import antonkozyriatskyi.circularprogressindicator.CircularProgressIndicator;

public class PlantSimilarityAdapter extends ArrayAdapter<Similarity> {

    List<Similarity> similarList;
    Context context;
    private CircularProgressIndicator.ProgressTextAdapter txtAdapter = new CircularProgressIndicator.ProgressTextAdapter() {
        @NonNull
        @Override
        public String formatText(double v) {
            return (int)v+"%";
        }
    };


    public PlantSimilarityAdapter(Context context, List<Similarity> similarList)
    {
        super(context, 0, similarList);
        this.context=context;
        this.similarList=similarList;
        Toast.makeText(context,similarList.size()+"",Toast.LENGTH_LONG).show();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ViewHolder viewHolder = new ViewHolder();
        final Similarity similarity=getItem(position);
        final Plant plant =similarity.getPlant();
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.singleitemtemplate, parent, false);
        }
        // Lookup view for data population
        viewHolder.commonName =  convertView.findViewById(R.id.common_name);
        viewHolder.imgCount = convertView.findViewById(R.id.imgCount);
        viewHolder.img_plant= convertView.findViewById(R.id.img_plant);
        viewHolder.circularProgress=convertView.findViewById(R.id.circularProgress);
        viewHolder.txtFamilyAndCommonName=convertView.findViewById(R.id.txtFamilyAndCommonName);
        // Populate the data into the template view using the data object


        viewHolder.commonName.setText(plant.getCommonName());
        viewHolder.imgCount.setText(plant.getGallery().size()+"");
        viewHolder.txtFamilyAndCommonName.setText(plant.getFamily()+", "+plant.getGenus());
        viewHolder.circularProgress.setMaxProgress(100);
        viewHolder.circularProgress.setProgressTextAdapter(txtAdapter);

        double match =Double.parseDouble(similarity.getSimilarity());
        viewHolder.circularProgress.setCurrentProgress(match);


        Picasso.get().load(plant.getImageUrl())
                .placeholder(R.drawable.ic_launcher_background)
                .into(viewHolder.img_plant);
        // Return the completed view to render on screen
        return convertView;
    }

    @Override
    public int getCount()
    {
        return this.similarList==null?0:similarList.size();
    }
    @Override
    public Similarity getItem(int position)
    {
        return this.similarList.get(position);
    }
    public Context getContext()
    {return this.context;}

    static class ViewHolder {
        ImageView img_plant;
        TextView commonName;
        TextView imgCount;
        TextView txtFamilyAndCommonName;
        CircularProgressIndicator circularProgress;
    }
}
