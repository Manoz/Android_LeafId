package com.example.manoj.upload_leaf.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.manoj.upload_leaf.R;
import com.example.manoj.upload_leaf.SinglePlantIDetailViewActivity;
import com.example.manoj.upload_leaf.interfaces.OnPlantDataReceivedListener;
import com.example.manoj.upload_leaf.model.Plant;
import com.squareup.picasso.Picasso;

public class PlantDetailShowFragment extends Fragment {



    private TextView kingdom,subkingdom,superDivision,division,phylum,className,subClass,order,family,genus,species,commonName,description;
    private ImageView img;
    private Plant plant;
    private OnPlantDataReceivedListener plantDataReceivedListener;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if(activity instanceof SinglePlantIDetailViewActivity) {
            plantDataReceivedListener = (OnPlantDataReceivedListener)activity;
            this.plant=plantDataReceivedListener.getPlantData();
        } else {
            // Throw an error!
        }
    }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view =inflater.inflate(R.layout.detailtemplate, container, false);

            //initialize view components
            img=view.findViewById(R.id.imageView2);
            kingdom= view.findViewById(R.id.txt_kingdom);
            subkingdom=view.findViewById(R.id.txt_subkingdom);
            superDivision=view.findViewById(R.id.txt_superdivision);
            family=view.findViewById(R.id.txt_family);
            genus=view.findViewById(R.id.txt_genus);
            species=view.findViewById(R.id.txt_species);
            commonName=view.findViewById(R.id.txt_commonname);
            division=view.findViewById(R.id.txt_division);
            phylum=view.findViewById(R.id.txt_phylum);
            subClass=view.findViewById(R.id.txt_subclass);
            order=view.findViewById(R.id.txt_order);
            className=view.findViewById(R.id.txt_classname);
            description=view.findViewById(R.id.txt_description);

            updateView();

            return view;
        }


        public void updateView()
        {
            Picasso.get().load(plant.getImageUrl())
                    .placeholder(R.drawable.ic_launcher_background)
                    .into(img);

            kingdom.setText(plant.getKingdom()+"");
            subkingdom.setText(plant.getSubkingdom()+"");
            superDivision.setText(plant.getDivision()+"");
            family.setText(plant.getFamily()+"");
            genus.setText(plant.getGenus()+"");
            species.setText(plant.getSpecies()+"");
            commonName.setText(plant.getCommonName()+"");
            division.setText(plant.getDivision()+"");
            phylum.setText(plant.getPhylum()+"");
            subClass.setText(plant.getSubClass()+"");
            order.setText(plant.getOrder()+"");
            className.setText(plant.getClassName()+"");
            description.setText(plant.getDescription()+"");
        }
}

