package com.example.manoj.upload_leaf.views;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.example.manoj.upload_leaf.R;

public class CameraClickButton extends AppCompatImageView {
    Context context;
    private OnTouchListener cameraBtnTouchListener = new OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            int setIcon=Icons.IC_RELEASED;

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    setIcon=Icons.IC_PRESSED;
                    break;
                }
                case MotionEvent.ACTION_UP: {
                    break;
                }
                case MotionEvent.ACTION_BUTTON_PRESS:
                    setIcon=Icons.IC_PRESSED;
                    break;
                case MotionEvent.ACTION_BUTTON_RELEASE:
                    setIcon=Icons.IC_RELEASED;
                    break;
                default:
                    break;
            }
            setImage(setIcon);
            return false;
        }
    };
    private class Icons{
        public static final int IC_RELEASED= R.drawable.icon_camshutter;
        public static final int IC_PRESSED=R.drawable.icon_camshutterpressed;
    }
    public CameraClickButton(Context context)
    {
        this(context,null);
        this.setOnTouchListener(cameraBtnTouchListener);
    }
    public CameraClickButton(Context context, AttributeSet attrs){
        super(context,attrs);
        this.context=context;
        setImage(Icons.IC_RELEASED);
    }

    public void setImage(int i)
    {
        super.setImageResource(i);
    }

}
