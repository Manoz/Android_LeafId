package com.example.manoj.upload_leaf.interfaces;

public interface OnFlashButtonPressed  {
     void onFlashSetAuto();
     void onFlashSetOn();
     void onFlashSetOff();
}
