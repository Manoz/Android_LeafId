package com.example.manoj.upload_leaf.model;

import java.util.List;

public class Plant
{
private String id;
private String kingdom,subkingdom,imageUrl,superDivision,division,phylum,className,subClass,order,family,genus,species,commonName,description;
private List<String> gallery;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSuperDivision() {
        return superDivision;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public void setSuperDivision(String superDivision) {
        this.superDivision = superDivision;
    }

    public String getPhylum() {
        return phylum;
    }

    public void setPhylum(String phylum) {
        this.phylum = phylum;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSubClass() {
        return subClass;
    }

    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getGenus() {
        return genus;
    }

    public void setGenus(String genus) {
        this.genus = genus;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    public String getKingdom() {
        return kingdom;
    }

    public void setKingdom(String kingdom) {
        this.kingdom = kingdom;
    }

    public String getSubkingdom() {
        return subkingdom;
    }

    public void setSubkingdom(String subkingdom) {
        this.subkingdom = subkingdom;
    }


    public String toString()
    {
        String view;
        view =  "Kingdom ="+this.kingdom+"\n"+
                " Sub Kingdom ="+this.subkingdom+"\n"+
                " Super Division ="+this.superDivision+"\n"+
                " Division ="+this.division+"\n"+
                " Phylum ="+this.phylum+"\n"+
                " Class ="+this.className+"\n"+
                " Sub class ="+this.subClass+"\n"+
                " Order ="+this.order+"\n"+
                " Genus ="+this.genus+"\n"+
                " Species ="+this.species+"\n"+
                " Common Name ="+this.commonName+"\n"+
                " Description ="+this.description+"\n"+
                " Gallery = "+ this.gallery;

        return view;
    }

}
