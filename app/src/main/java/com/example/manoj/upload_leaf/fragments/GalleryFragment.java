package com.example.manoj.upload_leaf.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.example.manoj.upload_leaf.R;
import com.example.manoj.upload_leaf.SinglePlantIDetailViewActivity;
import com.example.manoj.upload_leaf.adapters.ImageGalleryAdapter;
import com.example.manoj.upload_leaf.interfaces.OnPlantDataReceivedListener;
import com.example.manoj.upload_leaf.model.Plant;


public class GalleryFragment extends Fragment {
    private OnPlantDataReceivedListener plantDataReceivedListener;
    private Plant plant;
    private RecyclerView recyclerView;
    private Activity parentActivity;
    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        this.parentActivity=activity;

        if(activity instanceof SinglePlantIDetailViewActivity) {
            plantDataReceivedListener = (OnPlantDataReceivedListener)activity;
            this.plant=plantDataReceivedListener.getPlantData();
        } else {
            // Throw an error!
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_gallary_recycler_view, container, false);

        //make up data
        String[] imgLinks=new String[plant.getGallery().size()];
        imgLinks=plant.getGallery().toArray(imgLinks);
        recyclerView= view.findViewById(R.id.rv_image) ;
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this.parentActivity, 2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        ImageGalleryAdapter adapter = new ImageGalleryAdapter(this.parentActivity,imgLinks);
        recyclerView.setAdapter(adapter);



        return view;
    }

}