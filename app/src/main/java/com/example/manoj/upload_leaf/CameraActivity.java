package com.example.manoj.upload_leaf;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.ImageView;

import com.example.manoj.upload_leaf.camera.PhotoHandler;

import com.example.manoj.upload_leaf.interfaces.OnFlashButtonPressed;
import com.example.manoj.upload_leaf.views.CameraClickButton;
import com.example.manoj.upload_leaf.views.FlashButton;

import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;


public class CameraActivity extends AppCompatActivity {

    private byte[] mCameraData;
    private ImageView ic_cancel,ic_swapcamera;
    private CameraClickButton ic_clickbtn;
    private CameraView cameraView;
    private FlashButton flashButton;

    private CameraKitEventListener cameraKitEventListener=new CameraKitEventListener() {
        @Override
        public void onEvent(CameraKitEvent cameraKitEvent) {

        }

        @Override
        public void onError(CameraKitError cameraKitError) {

        }

        @Override
        public void onImage(CameraKitImage cameraKitImage) {
            mCameraData=cameraKitImage.getJpeg();
            if (mCameraData != null) {
                Uri uri= saveImage();
                Intent resultIntent = new Intent();
                resultIntent.setData(uri);
                setResult(Activity.RESULT_OK, resultIntent);
            } else {
                setResult(Activity.RESULT_CANCELED);
            }
            finish();
        }

        @Override
        public void onVideo(CameraKitVideo cameraKitVideo) {

        }
    };
    private OnClickListener onCancelClicked= new OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };
    private OnClickListener onPictureSnapped = new OnClickListener() {
        @Override
        public void onClick(View v) {
            cameraView.captureImage();
        }
    };
    private OnFlashButtonPressed flashButtonPressed = new OnFlashButtonPressed() {
        @Override
        public void onFlashSetAuto() {
            cameraView.setFlash(CameraKit.Constants.FLASH_AUTO);
            flashButton.setState(0);
        }

        @Override
        public void onFlashSetOn() {
            cameraView.setFlash(CameraKit.Constants.FLASH_OFF);
            flashButton.setState(1);
        }

        @Override
        public void onFlashSetOff() {
            cameraView.setFlash(CameraKit.Constants.FLASH_ON);
            flashButton.setState(2);
        }
    };


    private OnClickListener onCameraSwappedBtnClicked = new OnClickListener() {
        @Override
        public void onClick(View view) {
            cameraView.toggleFacing();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_camera);

        ic_clickbtn=findViewById(R.id.ic_clickbtn);
        flashButton=findViewById(R.id.ic_flash);
        ic_cancel=findViewById(R.id.ic_cancel);
        ic_swapcamera=findViewById(R.id.ic_swapcamera);
        cameraView = findViewById(R.id.camera);

        cameraView.addCameraKitListener(cameraKitEventListener);
        ic_clickbtn.setOnClickListener(onPictureSnapped);

        flashButton.setContext(CameraActivity.this);
        flashButton.setOnFlashButtonPressedListener(flashButtonPressed);
        ic_swapcamera.setOnClickListener(onCameraSwappedBtnClicked);
        ic_cancel.setOnClickListener(onCancelClicked);

    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraView.start();
    }

    @Override
    protected void onPause() {
        cameraView.stop();
        super.onPause();
    }

    public Uri saveImage()
    {
        PhotoHandler handler = new PhotoHandler(CameraActivity.this);
        return handler.saveImage(mCameraData);
    }

}
