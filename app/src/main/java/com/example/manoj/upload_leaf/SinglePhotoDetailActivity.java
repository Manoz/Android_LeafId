package com.example.manoj.upload_leaf;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.manoj.upload_leaf.adapters.FullScreenImageAdapter;

public class SinglePhotoDetailActivity extends AppCompatActivity {
private int originalIndex=0;
private String[] imgLinks;
private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_photo_detail);

        pager =findViewById(R.id.pager);


        Bundle b=this.getIntent().getExtras();
        if(b!=null)
        {
            imgLinks=b.getStringArray("images");
            originalIndex=b.getInt("index");
        }
        FullScreenImageAdapter  adapter = new FullScreenImageAdapter(SinglePhotoDetailActivity.this,imgLinks);
        pager.setAdapter(adapter);
        pager.setCurrentItem(originalIndex);
    }
}
