package com.example.manoj.upload_leaf;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.Toast;


import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.example.manoj.upload_leaf.util.MySingleton;

import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialView;
import com.sketchproject.infogue.modules.VolleyMultipartRequest;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private ImageView btn_camera, btn_gallery;

    private final int GALLERY_IMG_REQUEST = 1;
    public static final int CUSTOM_CAMERA_REQUEST=2;
    public static String RESULT_NAME="epuzzle";

    private Bitmap bitmap;

    private String checkURL = "https://plantrecognition.herokuapp.com/api/check";
    private String getAllURL = "https://plantrecognition.herokuapp.com/api/find";

    //speeddial
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int ADD_ACTION_POSITION = 4;
    private SpeedDialView mSpeedDialView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //ask for permission
        isGrantExternalRW(getApplicationContext());

        //set layouts
        setContentView(R.layout.activity_main);
        btn_camera = findViewById(R.id.btn_camera);
        btn_gallery = findViewById(R.id.btn_gallery);
        btn_camera.setOnClickListener(this);
        btn_gallery.setOnClickListener(this);
        initSpeedDial(savedInstanceState == null);
    }

    private void initSpeedDial(boolean addActionItems) {
        mSpeedDialView = findViewById(R.id.speedDial);

        if (addActionItems) {
            mSpeedDialView.addActionItem(new SpeedDialActionItem.Builder(R.id.fab_add_database, R.drawable
                    .icons_database)
                    .setFabBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent,
                            getTheme()))
                    .setLabel(R.string.label_database)
                    .setLabelColor(Color.BLACK)
                    .setLabelBackgroundColor(getResources().getColor(R.color.colorAccent))
                    .create());

            mSpeedDialView.addActionItem(new SpeedDialActionItem.Builder(R.id.fab_add_gallery, R.drawable
                    .icon_fabgallery)
                    .setFabBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent,
                            getTheme()))
                    .setLabel(R.string.label_gallery)
                    .setLabelColor(Color.BLACK)
                    .setLabelBackgroundColor(getResources().getColor(R.color.colorAccent))
                    .create());

            mSpeedDialView.addActionItem(new SpeedDialActionItem.Builder(R.id.fab_add_camera, R.drawable
                    .icon_fabcamera)
                    .setFabBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent,
                            getTheme()))
                    .setLabel(getString(R.string.label_camera))
                    .setLabelColor(Color.BLACK)
                    .setLabelBackgroundColor(getResources().getColor(R.color.colorAccent))
                    .create());

            mSpeedDialView.addActionItem(new SpeedDialActionItem.Builder(R.id.fab_add_shareapp, R.drawable
                    .icon_share)
                    .setFabBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent,
                            getTheme()))
                    .setLabel(getString(R.string.label_shareapp))
                    .setLabelColor(Color.BLACK)
                    .setLabelBackgroundColor(getResources().getColor(R.color.colorAccent))
                    .create());

            mSpeedDialView.addActionItem(new SpeedDialActionItem.Builder(R.id.fab_add_aboutus, R.drawable
                    .icon_aboutus)
                    .setFabBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent,
                            getTheme()))
                    .setLabel(getString(R.string.label_aboutus))
                    .setLabelColor(Color.BLACK)
                    .setLabelBackgroundColor(getResources().getColor(R.color.colorAccent))
                    .create());
        }

        //Set main action clicklistener.
        mSpeedDialView.setOnChangeListener(new SpeedDialView.OnChangeListener() {
            @Override
            public boolean onMainActionSelected() {
                //showToast("Main action clicked!");
                return false; // True to keep the Speed Dial open
            }

            @Override
            public void onToggleChanged(boolean isOpen) {
                Log.d(TAG, "Speed dial toggle state changed. Open = " + isOpen);
            }
        });

        //Set option fabs clicklisteners.
        mSpeedDialView.setOnActionSelectedListener(new SpeedDialView.OnActionSelectedListener() {
            @Override
            public boolean onActionSelected(SpeedDialActionItem actionItem) {
                switch (actionItem.getId()) {
                    case R.id.fab_add_database:
                        getAllPlantsFromServer();
                        Toast.makeText(MainActivity.this, ""+"database", Toast.LENGTH_SHORT).show();
                        return false;
                    case R.id.fab_add_gallery:
                        Toast.makeText(MainActivity.this, ""+"gallery", Toast.LENGTH_SHORT).show();
                        return false;
                    case R.id.fab_add_camera:
                        Intent intent = new Intent(MainActivity.this,CameraActivity.class);
                        startActivityForResult(intent, CUSTOM_CAMERA_REQUEST);
                        return false;
                    case R.id.fab_add_shareapp:
                        Toast.makeText(MainActivity.this, ""+"ShareApp", Toast.LENGTH_SHORT).show();
                        return false;
                    case R.id.fab_add_aboutus:
                        setContentView(R.layout.activity_about_us);
                        return false;
                    default:
                        break;
                }
                return true; // To keep the Speed Dial open
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        //Closes menu if its opened.
        if (mSpeedDialView.isOpen()) {
            mSpeedDialView.close();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_gallery:
                selectImageFromGallery();
                break;
            case R.id.btn_camera:
                Intent i = new Intent(MainActivity.this,CameraActivity.class);
                startActivityForResult(i,CUSTOM_CAMERA_REQUEST);
                break;

            default:
                break;
        }
    }

    //Select Image from Gallery

    private void selectImageFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_IMG_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if ((resultCode != RESULT_OK)||(data==null))
        {
            Toast.makeText(MainActivity.this,"null img recvd",Toast.LENGTH_LONG).show();
            return;
        }
        switch(requestCode)
        {
            case GALLERY_IMG_REQUEST:
                handleFromGallery(data);
                uploadImage();
                break;
            case CUSTOM_CAMERA_REQUEST:
            {
                Uri uri =data.getData();
                CropImage.activity(uri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            }

            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
            {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                handleFromCroppedImage(result.getUri());
                uploadImage();
                break;
            }
            case CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE:
            default:
                Toast.makeText(this,"Error occurred",Toast.LENGTH_LONG).show();
        }

    }


    private void uploadImage() {
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait uploading image...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        VolleyMultipartRequest multipartRequest =
                new VolleyMultipartRequest(Request.Method.POST,
                        checkURL,
                        new Response.Listener<NetworkResponse>() {

            @Override
            public void onResponse(NetworkResponse response1) {
                String resultResponse = new String(response1.data);
                progress.dismiss();

                redirectToSimilarityActivity(resultResponse);
                }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();

                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    System.out.println("Error");
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String message = response.getString("message");


                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                //params.put("api_token", "gh659gjhvdyudo973823tt9gvjf7i6ric75r76");

                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);

                DataPart dp = new VolleyMultipartRequest.DataPart();
                dp.setContent(stream.toByteArray());
                dp.setFileName("uploadfie.jpg");
                dp.setType("image/jpeg");
                params.put("file", dp);

                return params;
            }
        };

        MySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);

    }

    private void redirectToShowPlantListActivity(String jsonData) {

        Intent i = new Intent(getApplicationContext(), ShowPlantListActivity.class);
        i.putExtra(RESULT_NAME, jsonData);
        if(jsonData==null){
            showToast(getString(R.string.bad_response));
            return;
        }

        startActivity(i);


    }
    private void redirectToSimilarityActivity(String jsonData) {

        Intent i = new Intent(getApplicationContext(), SimilarActivity.class);
        i.putExtra("similarData", jsonData);
        if(jsonData==null){
            showToast(getString(R.string.bad_response));
            return;
            }
        startActivity(i);
    }

    public void getAllPlantsFromServer() {
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Loading data...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, getAllURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        progress.dismiss();

                        redirectToShowPlantListActivity(response1);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                showToast("ERROR "+error.networkResponse);
            }
        });
        MySingleton.getInstance(getBaseContext()).addToRequestQueue(stringRequest);
    }

public void handleFromGallery(Intent data)
{
    //check permission
    isGrantExternalRW(MainActivity.this.getApplicationContext());
    Uri selectedImage = data.getData();
    String[] filePathColumn = { MediaStore.Images.Media.DATA };
    Cursor cursor = getContentResolver().query(selectedImage,
            filePathColumn, null, null, null);
    cursor.moveToFirst();

    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
    String picturePath = cursor.getString(columnIndex);
    cursor.close();
    this.bitmap= BitmapFactory.decodeFile(picturePath);
}
public void handleFromCroppedImage(Uri uri){
        try {
            this.bitmap=MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
        }
        catch(FileNotFoundException e)
        {
            showToast(uri.getPath()+" not found.");
        }
        catch(IOException e)
        {
            showToast(e.getMessage());
        }

    }
    public  void isGrantExternalRW(Context context) {
        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
    }

    private void showToast(String message){
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
    }
}




