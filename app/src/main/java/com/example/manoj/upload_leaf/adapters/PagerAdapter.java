package com.example.manoj.upload_leaf.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.manoj.upload_leaf.fragments.GalleryFragment;
import com.example.manoj.upload_leaf.fragments.PlantDetailShowFragment;
import com.example.manoj.upload_leaf.fragments.WikiFragment;


public class PagerAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    private FragmentManager fm;
    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.fm=fm;
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
    Fragment returnFragment=null;
        switch (position) {
            case 0:
                returnFragment = new PlantDetailShowFragment();
                break;
            case 1:
                returnFragment = new GalleryFragment();
                break;
            case 2:
                returnFragment = new WikiFragment();
                break;
            default:
                returnFragment=null;
        }
        return returnFragment;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}