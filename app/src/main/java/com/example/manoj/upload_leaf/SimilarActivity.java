package com.example.manoj.upload_leaf;


import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.manoj.upload_leaf.ParserHelper.NetworkResponseParser;
import com.example.manoj.upload_leaf.adapters.PlantSimilarityAdapter;
import com.example.manoj.upload_leaf.model.Plant;
import com.example.manoj.upload_leaf.model.Similarity;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class SimilarActivity extends AppCompatActivity {
    List<Similarity> similarPlantList;
    PlantSimilarityAdapter similarityAdapter;
    ListView listView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_similar);
        listView = findViewById(R.id.similarPlantList);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            String value = extras.getString("similarData");
            try{
                NetworkResponseParser parser = new NetworkResponseParser();
                JSONArray jsonArr = new JSONArray(value);
                similarPlantList = parser.getSimilarPlantList(jsonArr);

                similarityAdapter = new PlantSimilarityAdapter(this, similarPlantList);
                listView.setAdapter(similarityAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position,
                                            long id) {
                        Context context=similarityAdapter.getContext();
                        Plant plant =similarityAdapter.getItem(position).getPlant();
                        Intent i = new Intent(context,  SinglePlantIDetailViewActivity.class);
                        i.putExtra("id", plant.getId());
                        context.startActivity(i);
                    }
                });
            }
            catch(JSONException e){

            }

        }
        else{
        }



    }
}
