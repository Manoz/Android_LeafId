package com.example.manoj.upload_leaf.views;


import android.content.Context;
import android.content.SharedPreferences;

import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;

import com.example.manoj.upload_leaf.R;
import com.example.manoj.upload_leaf.interfaces.OnFlashButtonPressed;

import static android.content.Context.MODE_PRIVATE;


public class FlashButton extends AppCompatImageView {
    private Context context;
    private static final String PREFS_NAME = "FlashPref";
    private static final String KEY_NAME="STATE";
    private OnFlashButtonPressed onFlashButtonPressed;
    private class IconSet{
        public  static final int IC_FLASH_ON=R.drawable.icon_flashon;
        public  static final int IC_FLASH_OFF=R.drawable.icon_flashoff;
        public  static final int IC_FLASH_AUTO=R.drawable.icon_flashauto;
    }
    private OnClickListener flashButtonOnClick = new OnClickListener() {
        int clickCount=getFlashState();
        @Override
        public void onClick(View view) {
            if(onFlashButtonPressed==null) return;
            clickCount=(++clickCount)%3;
            setFlashState(clickCount);
            switch(clickCount)
            {
                case 0:
                    onFlashButtonPressed.onFlashSetAuto();
                    break;
                case 1:
                    onFlashButtonPressed.onFlashSetOn();
                    break;
                case 2:
                    onFlashButtonPressed.onFlashSetOff();
                    break;
                    default:
                        onFlashButtonPressed.onFlashSetAuto();

            }

        }
    };

public enum States{
    FLASH_AUTO,
    FLASH_OFF,
    FLASH_ON
}

    public FlashButton(Context context)
    {
        this(context,null);

    }
    public FlashButton(Context context, AttributeSet attrs){
        super(context,attrs);
        this.context=context;
        setState(getFlashState());
        super.setOnClickListener(flashButtonOnClick);
    }


    public void setState(int state){
        int setIcon;
        States whichState =States.values()[state];
        switch(whichState)
        {
            case FLASH_AUTO:
                setIcon=IconSet.IC_FLASH_AUTO;
                break;
            case FLASH_ON:
                setIcon=IconSet.IC_FLASH_ON;
                break;
            case FLASH_OFF:
                setIcon=IconSet.IC_FLASH_OFF;
                break;
                default:
                    setIcon=IconSet.IC_FLASH_AUTO;
        }
        super.setImageResource(setIcon);
    }
private void setFlashState(int state)
{
    if(context==null){
       return;
    }
    SharedPreferences.Editor editor = this.context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
    editor.putInt(KEY_NAME, state);
    editor.apply();
}
private int getFlashState()
{
    if(this.context==null) return 0;

    SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
    return prefs.getInt(KEY_NAME,0);
}

public void setOnFlashButtonPressedListener(OnFlashButtonPressed l)
{
    this.onFlashButtonPressed=l;
}

public void setContext(Context context)
{
    this.context=context;
}

static  class ViewHolder
{

}
}
