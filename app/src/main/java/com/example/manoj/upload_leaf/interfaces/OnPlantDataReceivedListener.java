package com.example.manoj.upload_leaf.interfaces;

import com.example.manoj.upload_leaf.model.Plant;

public interface OnPlantDataReceivedListener {
    public Plant getPlantData();
}
